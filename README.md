# IEEE 802.11p on Linux

## Installation
This changes to fully support IEEE 802.11p were tested in Debian Bullseye (Debian 11).
To setup the current host for 802.11p, do the following steps:
1. Clone this repository and enter directory:  
    - `git clone https://gitlab.com/vehicle2x/11p-on-linux.git`  
    - `cd 11p-on-linux`
2. Install dependencies (Ansible):  
    - `apt install ansible`
3. Start builing and installing:
    - `cd ansible`
    - In case you're logged in as **root**, run:  
    `ansible-playbook example-playbook.yml`
    - If logged in as normal user with root privileges, run:  
    `ansible-playbook --ask-become-pass example-playbook.yml`

If everything goes as planned, you should be able to configure the wireless interfaces for IEEE 802.11p.  
If `ansible-playbook` return some error, please check [troubleshooting](#troubleshooting)

## Configure WLANs
To configure wlans, just run the script available at [scripts](scripts).
If you want to run this script at boot time, do:
- Copy and paste `configure_wlan` at `/etc/init.d`
- Run `update-rc.d configure_wlan defaults` to add /etc/init.d/configure_wlan to the boot sequence.

## Troubleshooting
**Is iw up to date and knows about OCB?**  
Check `iw | grep -i ocb`. If not, check `iw` version and install a more recent version (version 4.0 or later).

**Is the CRDA configured correctly?**  
Check `/sbin/regdbdump /lib/crda/regulatory.bin`
If the output does not have this lines in the first country. Then something went wrong when installing `wireless-regdb`.
```
$ /sbin/regdbdump /lib/crda/regulatory.bin
country 00: DFS-UNSET
        ...
        (5855.000 - 5865.000 @ 10.000), (N/A), (N/A)
        (5865.000 - 5875.000 @ 10.000), (23.00), (N/A)
        (5875.000 - 5885.000 @ 10.000), (33.00), (N/A)
        (5885.000 - 5895.000 @ 10.000), (23.00), (N/A)
        (5895.000 - 5905.000 @ 10.000), (33.00), (N/A)
        (5905.000 - 5915.000 @ 10.000), (N/A), (N/A)
        (5915.000 - 5925.000 @ 10.000), (N/A), (N/A)
        ...
```
To fix that, re-run the ansible-playbook tagged with reinstall-regdb: `ansible-playbook example-playbook.yml --tags reinstall-regdb`. Or you could build and install `wireless-regdb` manually.
    

